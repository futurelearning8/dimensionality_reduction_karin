import pandas as pd
import numpy as np
from tensorflow.keras.datasets import mnist
from sklearn.neighbors import KNeighborsClassifier
from sklearn.metrics import accuracy_score
from sklearn.decomposition import PCA
import time
import plotly.graph_objects as go


def print_metrices(actual_y, pred_y):
    knn_acc = accuracy_score(actual_y, pred_y)
    print(f'KNN classification model reached accuracy of: {knn_acc}\n')


def check_knn_with_pca_variance(train_X, train_y, test_X, test_y):
    knn_classifier = KNeighborsClassifier(3)
    variance = np.arange(0.3, 1, 0.05)
    knn_results = []

    # Runnimg all variances
    for v in variance:
        # fit PCA accordingly
        pca = PCA(n_components=v)
        pca.fit(train_X)

        # transform the data
        projected_train_X = pca.transform(train_X)
        project_test_X = pca.transform(test_X)

        # fit a KNN model with the projected
        start = time.time()
        knn_classifier.fit(projected_train_X, train_y)
        end = time.time()

        # prediction on the projected test and calc accuracy
        knn_pred_pca = knn_classifier.predict(project_test_X)
        knn_acc = accuracy_score(test_y, knn_pred_pca)
        knn_results.append([v, knn_acc, end-start])
        print(f'Variance:{v}, Accuracy:{knn_acc}, Training Time:{end-start}')

    df_knn_results = pd.DataFrame(knn_results, columns=['Variance', 'Accuracy', 'Training Time'])
    return df_knn_results


def plot_accuracy(df_accuracy):
    fig = go.Figure()
    # Add traces
    fig.add_trace(go.Scatter(x=df_accuracy['Variance'], y=df_accuracy['Accuracy'],
                             mode='lines+markers',
                             name='Accuracy'))
    fig.add_trace(go.Scatter(x=df_accuracy['Variance'], y=df_accuracy['Training Time'],
                             mode='lines+markers',
                             name='KNN Training Time'))

    fig.update_layout(
        title="KNN Performance",
        xaxis_title="Variance"
    )

    fig.show()


def main():
    (train_X, train_y), (test_X, test_y) = mnist.load_data()
    train_X = train_X.reshape((train_X.shape[0], train_X.shape[1]*train_X.shape[2]))
    test_X = test_X.reshape((test_X.shape[0], test_X.shape[1]*test_X.shape[2]))

    # 2. KNN classifier training - 0.9705
    print("KNN results:")
    knn_classifier = KNeighborsClassifier(3)
    knn_classifier.fit(train_X, train_y)
    knn_pred = knn_classifier.predict(test_X)
    print_metrices(test_y, knn_pred)

    # 3. PCA
    pca = PCA(n_components=0.95)
    pca.fit(train_X)

    # 4. pca projection - 0.9722
    print("KNN results after PCA:")
    projected_train_X = pca.transform(train_X)
    project_test_X = pca.transform(test_X)
    knn_classifier.fit(projected_train_X, train_y)
    knn_pred_pca = knn_classifier.predict(project_test_X)
    print_metrices(test_y, knn_pred_pca)

    # 5. KNN picks neighbors which very close to the tested point, and gives a prediction according them.
    #    when training with a lot of features, it's might me harder. So training with meaningful features make KNN
    #    Perform better

    # 6.
    df_knn_results = check_knn_with_pca_variance(train_X, train_y, test_X, test_y)

    # 7.
    plot_accuracy(df_knn_results)


if __name__ == '__main__':
    main()
